/* datetime.cpp
 * Copyright © 2020 Daniel McLaughlin
 *
 * The file is part of the Ceastar application.
 *
 * Ceastar is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Ceastar is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * Ceastar. If not, see <https://www.gnu.org/licenses/>.
 */

// Include the application header
#include "ceastar.h"

// Include the required system-headers
#include <swephexp.h>


// Default constructor and destructor
DateTime::~DateTime() {
  this->m_dJulianDay = 0;
}
DateTime::DateTime() {
  this->m_dJulianDay = 0;
}

// Other Constructors
DateTime::DateTime(const double dJulianDay) {
  this->m_dJulianDay = dJulianDay;
}
DateTime::DateTime(const int nYear, const int nMonth, const int nDay,
                                                   const double dHour) {
  int nCalFlag;
  if (nYear < 1582) {
    nCalFlag = SE_JUL_CAL;
  } else if (nYear > 1582) {
    nCalFlag = SE_GREG_CAL;
  } else {
    if (nMonth < 10) {
      nCalFlag = SE_JUL_CAL;
    } else if (nMonth > 10) {
      nCalFlag = SE_GREG_CAL;
    } else {
      if (nDay < 15) {
        nCalFlag = SE_JUL_CAL;
      } else {
        nCalFlag = SE_GREG_CAL;
      }
    }
  }
  this->m_dJulianDay = swe_julday(nYear, nMonth, nDay, dHour, nCalFlag);
}

// Static "constructors" that "construct" from either Gregorian or
// Julian dates
DateTime DateTime::FromJulian(const int nYear, const int nMonth,
                                   const int nDay, const double dHour) {
  DateTime lRet = DateTime(
                    swe_julday(nYear, nMonth, nDay, dHour, SE_JUL_CAL)
                  );
  return lRet;
}
DateTime DateTime::FromGregorian(const int nYear, const int nMonth,
                                   const int nDay, const double dHour) {
  DateTime lRet = DateTime(
                    swe_julday(nYear, nMonth, nDay, dHour, SE_GREG_CAL)
                  );
  return lRet;
}

// Function to extract the date (Gregorian or Julian) from the current
// instance Julian-day
int DateTime::GetDate(int *pnYear, int *pnMonth, int *pnDay,
                               double *pdHour, bool blGregorian) const {
  int nGreg = (blGregorian) ? 1 : 0;
  swe_revjul(this->m_dJulianDay, nGreg, pnYear, pnMonth, pnDay, pdHour);
  return 0;
}

// Functions to get and set the julian-day of the time
double DateTime::GetTime() const {
  return this->m_dJulianDay;
}
void DateTime::SetTime(const double dJulianDay) {
  this->m_dJulianDay = dJulianDay;
}

// Function to apply the time-zone to the current instance DateTime
void DateTime::ApplyTimeZone(const int nHour, const int nMinute,
                                                    const int nSecond) {
  double dOffset;
  dOffset =
    (double)nHour + ((double)nMinute / 60) + ((double)nSecond / 3600);
  dOffset /= (double)24;
  this->m_dJulianDay -= dOffset;
}

