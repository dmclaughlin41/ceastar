/* point.h
 * Copyright © 2020 Daniel McLaughlin
 *
 * The file is part of the Ceastar application.
 *
 * Ceastar is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Ceastar is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * Ceastar. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ___POINT_H__
#define ___POINT_H__

// Structure to Hold a Geographic Location
struct GeographicLocation {
  double Latitude;
  double Longitude;
  double Altitude;
};


// Structure to Hold a Chart-Point
struct ChartPoint {

  // The name of the point
  char *Name;

  // The points Body-ID (defined in SWEPH)
  int BodyId;

  // The zodical longitude and equatorial declination of the point
  double Longitude;
  double Declination;

  // The change-rate in longitude and declination, measured in degrees
  // per day
  double LongitudeSpeed;
  double DeclinationSpeed;


  // The default constructor
  ChartPoint();

  // Other constructors
  ChartPoint(
    int nBody
      /* The SWEPH Body-ID for the point */
  );
  ChartPoint(
    const char *szName,
      /* The name of the point */
    const double dLongitude
      /* The longitude of the point */
  );
  ChartPoint(
    const char *szName,
      /* The name of the point */
    const double dLongitude,
      /* The longitude of the point */
    const double dDeclination
      /* The declination of the point */
  );

  // Function to set the name of the point from a given SWEPH Body-ID
  void SetName(
    const int nBody
      /* The SWEPH Body-ID */
  );

  // Function to release the name-allocation for the point
  void ReleaseName(
    void
  );

  // Function to compute the point
  int Compute(
    const double dJulianDay,
      /* The julian-day to calculate for */
    GeographicLocation lLocation,
      /* The geographic location to compute from */
    int iBaseFlags
      /* The computational flags for SWEPH */
  );

  // Function to convert a normal degree on the Ecliptic to its
  // dodecatemoria
  static double DodecatemoriaDegree(
    const double dDegree
      /* The normal degree */
  ); // Returns the dodecatemoria degree

  // Functions to convert a normal degree on the Ecliptic to its
  // antiscia and contra-antiscia degrees
  static double AntisciaDegree(
    const double dDegree
      /* The normal degree */
  ); // Returns the antiscia degree
  static double ContraAntisciaDegree(
    const double dDegree
      /* The normal degree */
  ); // Returns the contra-antiscia degree

  // Function to compute the declination of a given point on the
  // Ecliptic
  static double ZodiacDeclination(
    const double dDegree,
      /* The zodical degree, measured from 0-Aries */
    const double dObliquity
      /* The axial-obliquity */
  ); // Returns the declination
};

#endif // ___POINT_H__

