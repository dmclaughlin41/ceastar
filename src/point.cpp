/* point.cpp
 * Copyright © 2020 Daniel McLaughlin
 *
 * The file is part of the Ceastar application.
 *
 * Ceastar is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Ceastar is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * Ceastar. If not, see <https://www.gnu.org/licenses/>.
 */

// Include the application header
#include "ceastar.h"

// Include the required system-headers
#include <cmath>
#include <swephexp.h>

// If not defined in "math.h", define M_PI now
#ifndef M_PI
#define M_PI (double)3.141592653589793238462643383279
#endif // M_PI

// Two functions to interconvert degrees and radians
double Deg2Rad(const double dDeg) {
  return dDeg * M_PI / (double)180;
}
double Rad2Deg(const double dRad) {
  return dRad * (double)180 / M_PI;
}



// Default Constructor
ChartPoint::ChartPoint() {
  this->Name = (char*)0;
  this->BodyId = -2;
  this->Longitude = (double)0;
  this->Declination = (double)0;
  this->LongitudeSpeed = (double)0;
  this->DeclinationSpeed = (double)0;
}

// Constructor using the SWEPH Body-ID as the basis for the point
ChartPoint::ChartPoint(int nBody) {
  this->BodyId = nBody;
  this->Longitude = (double)0;
  this->Declination = (double)0;
  this->LongitudeSpeed = (double)0;
  this->DeclinationSpeed = (double)0;
  this->SetName(nBody);
}

// Constructors taking the name, and predetermined longitude (and
// declination) of the point
ChartPoint::ChartPoint(const char *szName, const double dLongitude) {
  this->Name = (char*)malloc(sizeof(char) * (strlen(szName) + 1));
  strcpy(this->Name, szName);
  this->BodyId = -2;
  this->Longitude = dLongitude;
  this->Declination = (double)0;
  this->LongitudeSpeed = (double)0;
  this->DeclinationSpeed = (double)0;
}
ChartPoint::ChartPoint(const char *szName, const double dLongitude,
                                            const double dDeclination) {
  this->Name = (char*)malloc(sizeof(char) * (strlen(szName) + 1));
  strcpy(this->Name, szName);
  this->BodyId = -2;
  this->Longitude = dLongitude;
  this->Declination = dDeclination;
  this->LongitudeSpeed = (double)0;
  this->DeclinationSpeed = (double)0;
}


// Functions to set and release the name of the point
void ChartPoint::SetName(const int nBody) {
  char *szName;
  szName = (char*)malloc(sizeof(char) * 256);
  swe_get_planet_name(nBody, szName);
  this->Name = (char*)malloc(sizeof(char) * (strlen(szName) + 1));
  strcpy(this->Name, szName);
  free(szName);
}
void ChartPoint::ReleaseName() {
  free(this->Name);
}

// Function to compute the position of the point
int ChartPoint::Compute(const double dJulianDay,
                         GeographicLocation lLocation, int iBaseFlags) {
  double adValues[6];
  char szError[256];
  int nFlag = iBaseFlags;

  // If Topocentric Flag is Set, Set the Geographic Location
  if ((iBaseFlags & SEFLG_TOPOCTR) == SEFLG_TOPOCTR) {
    swe_set_topo(lLocation.Longitude, lLocation.Latitude,
                 lLocation.Altitude);
  }

  if ((iBaseFlags & SEFLG_HELCTR) == SEFLG_HELCTR) {
    // Heliocentric Computation
    swe_calc_ut(dJulianDay, this->BodyId, nFlag, adValues, szError);
    this->Longitude        = adValues[0];
    this->LongitudeSpeed   = adValues[3];
    this->Declination      = adValues[1];
    this->DeclinationSpeed = adValues[4];
  } else {
    // Geo/Topo-centric Computation
    // Get the longitude data
    swe_calc_ut(dJulianDay, this->BodyId, nFlag, adValues, szError);
    this->Longitude        = adValues[0];
    this->LongitudeSpeed   = adValues[3];
    // Get the declination data
    nFlag |= SEFLG_EQUATORIAL;
    swe_calc_ut(dJulianDay, this->BodyId, nFlag, adValues, szError);
    this->Declination      = adValues[1];
    this->DeclinationSpeed = adValues[4];
  }
  return 0;
}

// Function to convert a degree on the Zodiac to its equivalent
// dodecatemoria degree
double ChartPoint::DodecatemoriaDegree(const double dDegree) {

  // Ensure that the working degree is within [0, 360)
  double dWork = dDegree;
  while (dWork < 0)    { dWork += 360; }
  while (dWork >= 360) { dWork -= 360; }

  // What is the degree and cusp of the sign within which degree falls?
  double dStart, dSignDegree;
  dStart = floor(dWork / (double)30);
  dStart *= (double)30;
  dSignDegree = dWork - dStart;

  // Compute the number of degrees from the cusp of the real sign to the
  // dodecatemoria degree. Get the dodecatemoria degree.
  double dDodec = dSignDegree * (double)12;
  dDodec += dStart;
  while (dDodec < 0)    { dDodec += 360; }
  while (dDodec >= 360) { dDodec -= 360; }

  // Return the dodecatemoria degree
  return dDodec;
}

// Functions to convert a degree on the Zodiac to its equivalent
// antiscia and contra-antiscia degrees
double ChartPoint::AntisciaDegree(const double dDegree) {
  double dRet = 180 - dDegree;
  while (dRet < 0)    { dRet += 360; }
  while (dRet >= 360) { dRet -= 360; }
  return dRet;
}
double ChartPoint::ContraAntisciaDegree(const double dDegree) {
  double dRet = 360 - dDegree;
  while (dRet < 0)    { dRet += 360; }
  while (dRet >= 360) { dRet -= 360; }
  return dRet;
}

// Function to compute the declination of a given zodiacal degree
double ChartPoint::ZodiacDeclination(const double dDegree,
                                              const double dObliquity) {
  // Convert the degrees to radians for working
  double dDeg, dObl;
  dDeg = Deg2Rad(dDegree);
  dObl = Deg2Rad(dObliquity);

  // Compute the declination of the zodical degree
  double dDec = sin(dDeg) * sin(dObl);
  dDec = asin(dDec);

  // Convert the declination back to degrees and return
  dDec = Rad2Deg(dDec);
  return dDec;
}

