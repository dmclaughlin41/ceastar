/* parse.cpp
 * Copyright © 2020 Daniel McLaughlin
 *
 * The file is part of the Ceastar application.
 *
 * Ceastar is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Ceastar is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * Ceastar. If not, see <https://www.gnu.org/licenses/>.
 */

// Include the application header
#include "ceastar.h"

// Include the required system-headers
#include <cmath>
#include <cstdio>
#include <cstring>


// Function to read and parse a formatted coordinate string
double ReadCoordinate(const char *szCoord) {

  // Set up to attempt to parse the input-string
  double dReturn;
  double dVal;
  int nVal;
  char cVal;
  int n;
  bool blSuccess = false;

  // Test format "Hd.ddd"
  if (sscanf(szCoord, "%c%lf", &cVal, &dVal) == 2) {
    if (cVal == 'N' || cVal == 'n' || cVal == 'E' || cVal == 'e') {
      dReturn = dVal;
      blSuccess = true;
    }
    if (cVal == 'S' || cVal == 's' || cVal == 'W' || cVal == 'w') {
      dReturn = -dVal;
      blSuccess = true;
    }
  }
  if (blSuccess)
    return dReturn;

  // If not above, then test format "dHm.mmm"
  if (sscanf(szCoord, "%d%c%lf", &nVal, &cVal, &dVal) == 3) {
    if (cVal == 'N' || cVal == 'n' || cVal == 'E' || cVal == 'e') {
      dReturn = (double)nVal + (dVal / 60);
      blSuccess = true;
    }
    if (cVal == 'S' || cVal == 's' || cVal == 'W' || cVal == 'w') {
      dReturn = (double)nVal + (dVal / 60);
      dReturn *= -1;
      blSuccess = true;
    }
  }
  if (blSuccess)
    return dReturn;

  // If still not above, then test format "d.ddd"
  if (sscanf(szCoord, "%lf", &dVal) == 1) {
    dReturn = dVal;
    blSuccess = true;
  }
  if (blSuccess)
    return dReturn;

  // Otherwise there is a formatting error, return zero
  return 0;
}

// Function to read a formatted date string
int ReadDate(const char* szDate, int *pnYear, int *pnMonth,
                                                           int *pnDay) {
  int nYear, nMonth, nDay;
  int n, nRet;
  n = sscanf(szDate, "%d-%d-%d", &nYear, &nMonth, &nDay);
  if (n == 3) {
    nRet = 0;
    *pnYear = nYear;
    *pnMonth = nMonth;
    *pnDay = nDay;
  } else {
    nRet = 1;
    *pnYear = 0;
    *pnMonth = 0;
    *pnDay = 0;
  }
  return nRet;
}

// Function to read a formatted time string
int ReadTime(const char* szDate, int *pnHour, int *pnMinute,
                                                        int *pnSecond) {
  // Set up to attempt to read and parse the time-string
  int n1, n2, n3;
  char c1, c2;
  int n;
  bool blValid = true;
  bool blSuccess = false;

  // Attempt to read format "hh:mm:ss"
  if (sscanf(szDate, "%d%c%d%c%d", &n1, &c1, &n2, &c2, &n3) == 5) {
    if (c1 == ':' && c2 == ':') {
      if (n2 < 0 || n2 > 59 || n3 < 0 || n3 > 59 || n1 < 0 || n1 > 23) {
        blValid =false;
      } else {
        *pnHour = n1;
        *pnMinute = n2;
        *pnSecond = n3;
        blSuccess = true;
      }
    } else {
      blValid = false;
    }
  }
  if (blSuccess)
    return 0;
  if (!blValid) {
    *pnHour = 0;
    *pnMinute = 0;
    *pnSecond = 0;
    return 1;
  }

  // Otherwise attepmt to read and parse format "hh:mm"
  if (sscanf(szDate, "%d%c%d", &n1, &c1, &n2) == 3) {
    if (c1 == ':') {
      if (n1 < 0 || n1 > 23 || n2 < 0 || n2 > 59) {
        blValid = false;
      } else {
        *pnHour = n1;
        *pnMinute = n2;
        *pnSecond = 0;
        blSuccess = true;
      }
    } else {
      blValid = false;
    }
  }
  if (blSuccess) {
    return 0;
  } else {
    *pnHour = 0;
    *pnMinute = 0;
    *pnSecond = 0;
    return 1;
  }
}

// Function to read a formatted time-zone string
int ReadTimeZone(const char* szTZ, int *pnHour, int *pnMinute,
                                                        int *pnSecond) {
  // If the first charactor of the time-zone is not either 'h' or 'm',
  // do not attempt to parse the input; stop here and return the error.
  if (
    !(
      szTZ[0] == 'h' || szTZ[0] == 'm' ||
      szTZ[0] == 'H' || szTZ[0] == 'M'
    )
  ) {
    *pnHour = 0;
    *pnMinute = 0;
    *pnSecond = 0;
    return 1;
  }

  // Otherwise, attempt to parse the input
  char c1, c2;
  int n1, n2, n3;
  double d;
  double dMeridian;
  bool blSuccess = false;
  bool blValid = true;
  bool blNeg;

  // If the input is given as hours
  if (szTZ[0] == 'h' || szTZ[0] == 'H') {
    // Attempt to read as format "hHhm"
    if (sscanf(szTZ, "%c%d%c%d", &c1, &n1, &c2, &n2) == 4) {
      if (n1 < 0 || n2 < 0 || n2 > 59) {
        blValid = false;
      } else if (c2 == 'E' || c2 == 'e') {
        *pnHour = n1;
        *pnMinute = n2;
        *pnSecond = 0;
        blSuccess = true;
      } else if (c2 == 'W' || c2 == 'w') {
        *pnHour = -n1;
        *pnMinute = -n2;
        *pnSecond = 0;
        blSuccess = true;
      } else {
        blValid = false;
      }
    }
    // Otherwise attempt to read format as "hHh"
    else if (sscanf(szTZ, "%c%d%c", &c1, &n1, &c2) == 3) {
      if (n1 < 0) {
        blValid = false;
      } else if (c2 == 'E' || c2 == 'e') {
        *pnHour = n1;
        *pnMinute = 0;
        *pnSecond = 0;
        blSuccess = true;
      } else if (c2 == 'W' || c2 == 'w') {
        *pnHour = -n1;
        *pnMinute = 0;
        *pnSecond = 0;
        blSuccess = true;
      } else {
        blValid = false;
      }
    }
    // Otherwise the format is invalid
    else {
      blValid = false;
    }
  }

  // Otherwise the input is given as meridian-degrees
  else {
    // Attempt to read as format "mdhm.mm"
    if (sscanf(szTZ, "%c%d%c%lf", &c1, &n1, &c2, &d) == 4) {
      if (n1 < 0 || d < 0 || d >= 60) {
        blValid = false;
      } else if (c2 == 'E' || c2 == 'e') {
        dMeridian = (double)n1 + (d / 60);
        blNeg = false;
        blSuccess = true;
      } else if (c2 == 'W' || c2 == 'w') {
        dMeridian = (double)n1 + (d / 60);
        blNeg = true;
        blSuccess = true;
      } else {
        blValid = false;
      }
      if (blSuccess) {
        dMeridian /= 360;
        dMeridian *= 24;
        n1 = (int)floor(dMeridian);
        dMeridian -= (double)n1;
        dMeridian *= 60;
        n2 = (int)floor(dMeridian);
        dMeridian -= (double)n2;
        dMeridian *= 60;
        n3 = (int)floor(dMeridian);
        if (blNeg) {
          n1 *= -1;
          n2 *= -1;
          n3 *= -1;
        }
        *pnHour = n1;
        *pnMinute = n2;
        *pnSecond = n3;
      }
    } else {
      blValid = false;
    }
  }

  // Was parsing successful or invalid? Return accordingly.
  if (!blValid)
    return 1;
  else if (!blSuccess)
    return 1;
  else
    return 0;
}

// Function to output zodiacal degree and minute from degrees
char* WriteZodicalDegree(char *szZodical, const double dDegree,
                                              const bool blRetrograde) {

  // Ensure that the given degree is properly within the range
  // of [0,360)
  double dWorking = dDegree;
  while (dWorking >= 360) { dWorking -= 360; }
  while (dWorking < 0) { dWorking += 360; }

  // Get the degree and minute of the zodic sign represented by the
  // degree
  int nDeg, nMin;
  char *szSign;
  szSign = (char*)malloc(sizeof(char) * 8);
  if (dWorking < 30) {
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "ARI");
  } else if (dWorking < 60) {
    dWorking -= 30;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "TAU");
  } else if (dWorking < 90) {
    dWorking -= 60;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "GEM");
  } else if (dWorking < 120) {
    dWorking -= 90;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "CAN");
  } else if (dWorking < 150) {
    dWorking -= 120;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "LEO");
  } else if (dWorking < 180) {
    dWorking -= 150;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "VIR");
  } else if (dWorking < 210) {
    dWorking -= 180;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "LIB");
  } else if (dWorking < 240) {
    dWorking -= 210;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "SCO");
  } else if (dWorking < 270) {
    dWorking -= 240;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "SAG");
  } else if (dWorking < 300) {
    dWorking -= 270;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "CAP");
  } else if (dWorking < 330) {
    dWorking -= 300;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "AQU");
  } else {
    dWorking -= 330;
    nDeg = (int)floor(dWorking);
    dWorking -= (double)nDeg;
    dWorking *= 60;
    nMin = (int)floor(dWorking);
    strcpy(szSign, "PIS");
  }

  // Write the zodiacal degree to the output string parameter
  if (blRetrograde)
    sprintf(szZodical, "%s %02d°%02dR", szSign, nDeg, nMin);
  else
    sprintf(szZodical, "%s %02d°%02d ", szSign, nDeg, nMin);

  // Release the sign-string and return the formatted zodical degree
  free(szSign);
  return szZodical;
}

// Function to output a degree in DM-format
char* WriteDegree(char *szDegree, const double dDegree) {
  int nDeg;
  double dMin;
  bool blNeg = (dDegree < 0);
  double dWork = dDegree;
  if (blNeg)
    dWork *= -1;
  nDeg = (int)floor(dWork);
  dWork -= (double)nDeg;
  dWork *= (double)60;
  dMin = floor(dWork * 100);
  dMin /= 100;
  if (blNeg)
    sprintf(szDegree, "-%02d°%05.2lf", nDeg, dMin);
  else
    sprintf(szDegree, "+%02d°%05.2lf", nDeg, dMin);
  return szDegree;
}

// Function to output a degree in DMS-format
char* WritePreciseDegree(char *szDegree, const double dDegree) {

  int nDeg, nMin;
  double dSec;
  bool blNeg = (dDegree < 0);
  double dWork = dDegree;
  if (blNeg)
    dWork *= -1;
  nDeg = (int)floor(dWork);
  dWork -= nDeg;
  dWork *= 60;
  nMin = (int)floor(dWork);
  dWork -= nMin;
  dWork *= 60;
  dSec = floor(dWork * 100);
  dSec /= (double)100;
  if (blNeg)
    sprintf(szDegree, "-%02d°%02d'%05.2lf", nDeg, nMin, dSec);
  else
    sprintf(szDegree, "+%02d°%02d'%05.2lf", nDeg, nMin, dSec);
  return szDegree;
}

// Function to output an azimuth in D.DD-format
char* WriteAzimuth(char *szDegree, const double dDegree) {
  double dWork = dDegree;
  while (dWork < 0) { dWork += 360; }
  while (dWork >= 360) { dWork -= 360; }
  sprintf(szDegree, "%06.2lf", dWork);
  return szDegree;
}

