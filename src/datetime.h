/* datetime.h
 * Copyright © 2020 Daniel McLaughlin
 *
 * The file is part of the Ceastar application.
 *
 * Ceastar is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Ceastar is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * Ceastar. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ___DATETIME_H__
#define ___DATETIME_H__

// Include the application header
#include "ceastar.h"

// Class representing a date and time instance
class DateTime {
  private:

    // The Julian-Day of the DateTime
    double m_dJulianDay;

  public:

    // Default constructor and destructor
    ~DateTime();
    DateTime();

    // Constructor using the Julian Day
    DateTime(
      const double dJulianDay
        /* Julian-Day of the DateTime */
    );

    // Constructor using the date
    DateTime(
      const int nYear,
      const int nMonth,
      const int nDay,
      const double dHour
        /* Year, month, day, and hour of the DateTime */
        /* Gregorian after 15 Oct 1582, Julian before */
    );

    // Function to create a DateTime from a Julian date
    static DateTime FromJulian(
      const int nYear,
      const int nMonth,
      const int nDay,
      const double dHour
        /* The year, month, day, and hour of the date */
    );

    // Function to create a DateTime from a Gregorian date
    static DateTime FromGregorian(
      const int nYear,
      const int nMonth,
      const int nDay,
      const double dHour
        /* The year, month, day, and hour of the date */
    );

    // Function to get the date from the current instance DateTime
    int GetDate(
      int *pnYear,
      int *pnMonth,
      int *pnDay,
      double *pdHour,
        /* Integer and double-float pointers to contain the year, */
        /* month, day, and hour of the instance Julian-day        */
      bool blGregorian
        /* True for a Gregorian date, False for a Julian */
    ) const;

    // Functions to get and set the Julian-day of the current instance
    // DateTime
    double GetTime(
      void
    ) const;
    void SetTime(
      const double dJulianDay
        /* The Julian-day to set */
    );

    // Function to apply the time-zone offset to the current DateTime
    void ApplyTimeZone(
      const int nHour,
      const int nMinute,
      const int nSecond
        /* The hour, minute, and second of the offset */
        /* All positive for east-of-Greenwich offset, */
        /* All negative for west-of-Greenwich offset  */
    );
};

#endif // ___DATETIME_H__

