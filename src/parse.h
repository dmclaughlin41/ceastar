/* parse.h
 * Copyright © 2020 Daniel McLaughlin
 *
 * The file is part of the Ceastar application.
 *
 * Ceastar is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Ceastar is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * Ceastar. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ___PARSE_H__
#define ___PARSE_H__

// Include the application-header
#include "ceastar.h"

// Function to read and interpret a longitude/latitude coordinate
double ReadCoordinate(
  const char *szCoord
    /* The longitude/latitude string */
); // Returns the coordinate as a double-float, if successful

// Function to read and interpret a date
int ReadDate(
  const char* szDate,
    /* The date string */
  int *pnYear,
  int *pnMonth,
  int *pnDay
    /* Integer-pointers to contain the date on return */
);

// Function to read and interpret a time
int ReadTime(
  const char* szDate,
    /* The time string */
  int *pnHour,
  int *pnMinute,
  int *pnSecond
    /* Integer-pointers to contain the time on return */
);

// Function to read and interpret a time-zone
int ReadTimeZone(
  const char* szTZ,
    /* The time-zone string */
  int *pnHour,
  int *pnMinute,
  int *pnSecond
    /* Integer-pointers to contain the timezone-offset on return */
);

// Function to write a degree in zodical format
char* WriteZodicalDegree(
  char *szZodical,
    /* The string to contain the zodical degree on return */
  const double dDegree,
    /* The degree, measured from 0-ARIES */
  const bool blRetrograde
    /* True to include retrograde-R in string, False if not */
);

// Function to write a degree in DM-format
char* WriteDegree(
  char *szDegree,
    /* The string to contain the formatted degree on return */
  const double dDegree
    /* The degree */
);

// Function to write a degree in DMS-format
char* WritePreciseDegree(
  char *szDegree,
    /* The string to contain the formatted degree on return */
  const double dDegree
    /* The degree */
);

// Function to write an azimuth-degree in D.DD format
char* WriteAzimuth(
  char *szDegree,
    /* The string to contain the formatted degree on return */
  const double dDegree
    /* The degree */
);

#endif // ___PARSE_H__

