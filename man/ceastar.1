.TH CEASTAR 1 "24 September 2020" "" "Linux User Manual"
.SH NAME
ceastar - compute astrological charts
.SH SYNOPSIS
\fBceastar\fR [options]
.SH DESCRIPTION
Compute either geocentric, topocentric, or heliocentric astrological charts.
.PP
Mandatory arguments to long options are mandatory for short options too.
.TP
\fBLocational Arguments\fR
.IP "\fB--latitude\fR=\fILATITUDE\fR"
The latitude of the location
.IP "\fB--longitude\fR=\fILONGITUDE\fR"
The longitude of the location
.IP "\fB--altitude\fR=\fIALTITUDE\fR"
The altitude of the location
.IP "\fB--topocentric\fR"
Cast topocentric chart
.IP "\fB--heliocentric\fR"
Cast heliocentric chart
.IP "The latitude and longitude arguments are both required unless the heliocentric argument has been called, in which case both are forbidden. The altitude argument is not required but cannot be used if the heliocentric argument has been called."
.TP
\fBTiming Arguments\fR
.IP "\fB--date\fR=\fIDATE\fR"
The date of the chart
.IP "\fB--time\fR=\fITIME\fR"
The time of the chart
.IP "\fB--timezone\fR=\fITIMEZONE\fR"
The time-zone of the date-time of the chart
.IP "\fB--julian-day\fR=\fIJD\fR"
The Julian Day of the chart
.IP "\fB--julian-calendar\fR"
Use the Julian Calendar for dates after the Gregorian Calendar Reform
.IP "Either the \fB--date\fR and \fB--time\fR arguments both are required or the \fB--julian-day\fR argument is required. Both sets of arguments may not be used at the same time. The \fB--timezone\fR argument may only be used with the \fB--date\fR and \fB--time\fR arguments; it may not be used with the \fB--julian-day\fR argument."
.TP
\fBPoint-Selection Arguments\fR
.IP "\fB-o\fR, \fB--outer\fR"
Include the outer planets in the chart
.IP "\fB-a\fR, \fB--asteroid\fR"
Include the major asteroids in the chart
.IP "\fB-u\fR, \fB--uranian\fR"
Include the Uranian planets in the chart
.IP "The outer planets are: Uranus, Neptune, and Pluto. The asteroids are: Ceres, Pallas, Juno, Vesta, Pholus, and Chiron. The Uranian planets are: Cupido, Hades, Zeus, Kronos, Apollon, Admetos, Vulcanus, and Poseidon."
.TP
\fBComputational Arguments\fR
.IP "\fB--harmonic\fR=\fIHARMONIC\fR"
Return the chart with vibrational harmonic placements, rather than the actual placements
.IP "\fB--dodecatemoria\fR"
Return the chart with dodecatemoria placements, rather than the actual placements
.IP "\fB--antiscia\fR"
Return the chart with antiscia placements, rather than the actual placements
.IP "\fB--contra-antiscia\fR"
Return the chart with contra-antiscia placements, rather that the actual placements
.IP "\fB--sweph-directory\fR=\fIDIRECTORY\fR"
Provide the data-directory for the Swiss Ephemeris
.IP "The harmonic, dodecatemoria, and (contra)antiscia arguments are not required, but only one may be used at once. However, \fB--antiscia\fR and \fB--contra-antiscia\fR may both be used together. Note that if the antiscia and contra-antiscia arguments are both used, then the returned positions are actually the opposite positions from the actual positions."
.IP "If the Sweph-Directory is not provided, then Ceastar defaults to the Moshier Ephemeris."
.TP
\fBHelp and Version Arguments\fR
.IP "\fB-h\fR, \fB--help\fR"
Display the help for Ceastar
.IP "\fB-v\fR, \fB--version\fR"
Display the version for Ceastar
.SH FORMATTING
.TP
\fBLatitude and Longitude\fR
.PP
Latitude and longitude are entered in one of three formats, each: \fBD\fR, \fBZD\fR, and \fBDZM\fR, where \fID\fR is the number of degrees, \fIM\fR is the number of minutes, and \fIZ\fR is the hemisphere. Where the hemisphere is not explicitly stated in the format, positive numbers are understood as being degrees of north latitude or east longitude whereas negative numbers are understood as being degrees of south latitude or west longitude.
.PP
The hemisphere codes are:
.RS
\fBe\fR for \fIeast\fR
.RE
.RS
\fBw\fR for \fIwest\fR
.RE
.RS
\fBn\fR for \fInorth\fR
.RE
.RS
\fBs\fR for \fIsouth\fR
.RE
.PP
Examples in the format \fBD\fR:
.RS
\fB--latitude=25.45\fR is \fI25.45 degrees north latitude\fR
.RE
.RS
\fB--longitude=-95.75\fR is \fI95.75 degrees west longitude\fR
.RE
.PP
Examples in the format \fBZD\fR:
.RS
\fB--latitude=s17.8\fR is \fI17.8 degrees south latitude\fR
.RE
.RS
\fB--longitude=e45.9\fR is \fI45.9 degrees east longitude\fR
.RE
.PP
Examples in the format \fBDZM\fR:
.RS
\fB--latitude=34n12.8\fR is \fI34 degrees 12.8 minutes north latitude\fR
.RE
.RS
\fB--longitude=96w54.7\fR is \fI96 degrees 54.7 minutes west longitude\fR
.RE
.TP
\fBAltitude\fR
.PP
The altitude is always entered as a simple number, giving the elevation above sea-level in meters. Enter only the number, without the units, and always entered measured in meters.
.PP
Example of altitude:
.RS
\fB--altitude=250\fR is \fI250 meters above sea-level\fR
.RE
.TP
\fBDate and Time\fR
.PP
The date is always entered in the format: \fBYYYY-MM-DD\fR. The time is always entered in the format: \fBHH:mm(:ss)\fR with the hours entered in 24-Hour format; the seconds may be entered, but are not required.
.PP
Example of date and time:
.RS
\fB--date=2020-06-25\fR is \fI25 June 2020\fR
.RE
.RS
\fB--time=14:25\fR is \fI2:25 PM (14:25, 24-hour)\fR
.RE
.PP
The Julian Day is always entered as a simple number, giving the Julian Day.
.PP
Example of the Julian Day:
.RS
\fB--julian-day=2448616.23958\fR
.RE
.TP
\fBTime-Zone\fR
.PP
The time-zone is entered in one of two formats: \fBhours east or west of Greenwich\fR (useful for standardized time-zones) and \fBdegrees of meridian east or west of Greenwich\fR (useful for local merdian times).
.PP
Time-zones in hours are entered in the format: \fBhNZ(M)\fR where \fIN\fR is the number of hours, \fIZ\fR is either '\fBe\fR' for east or '\fBw\fR' for west of Greenwich, and \fIM\fR (not required) is the number of minutes in the hour. The initial "\fBh\fR" is required to distinguish the format as \fIhours\fR.
.PP
Examples in hour-format:
.RS
\fB--timezone=h3e\fR is \fIUTC+3:00\fR
.RE
.RS
\fB--timezone=h9w30\fR is \fIUTC-9:30\fR
.RE
.PP
Time-zones in degrees of meridian are entered in the format: \fBmNZM\fR where \fIN\fR is the number of degrees, \fIZ\fR is either '\fBe\fR' for east or '\fBw\fR' for west, and \fIM\fR is the number of minutes of arc within the degree. The initial "\fBm\fR" is required to distinguish the format as \fIdegrees of meridian\fR.
.PP
Examples in meridian-format:
.RS
\fB--timezone=m75w09\fR is \fIlocal meridian time at longitude 75 degrees 9 minutes west of Greenwich\fR
.RE
.RS
\fB--timezone=m44e00\fR is \fIlocal meridian time at longitude 44 degrees 0 minutes east of Greenwich\fR
.RE
.TP
\fBSweph\fR
.PP
The Sweph-Directory should be entered enclosed in quotes and be the full path for the Swiss Ephemeris data files (especially if the full path contains spaces). You should confirm the location for your own system, but most flavors of Linux should place it at "\fI/usr/share/libswe/ephe\fR".
.SH AUTHOR
Written by Daniel McLaughlin <\fIravenstar.8214@gmail.com\fR>.
.SH BUG REPORTING
If you encounter any utilization problems, please contact me at <\fIravenstar.8214@gmail.com\fR>.
.SH COPYRIGHT
Copyright © 2020 Daniel McLaughlin
.PP
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
.PP
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
.PP
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
