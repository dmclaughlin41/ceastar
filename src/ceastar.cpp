/* ceastar.cpp
 * Copyright © 2020 Daniel McLaughlin
 *
 * The file is part of the Ceastar application.
 *
 * Ceastar is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Ceastar is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * Ceastar. If not, see <https://www.gnu.org/licenses/>.
 */

// Include the application header
#include "ceastar.h"

// Include the required system headers
#include <cstdio>
#include <getopt.h>
#include <swephexp.h>

// Intra-Module Function Prototypes
int main(int argc, char *argv[]);
void ShowVersion(void);
void ShowHelp(void);


// Command-Line Options Structure
struct option longopts[] = {

  // Locational Arguments
  { "latitude",        required_argument, 0, 1000 },
  { "longitude",       required_argument, 0, 1001 },
  { "altitude",        required_argument, 0, 1002 },
  { "topocentric",     no_argument,       0, 1003 },
  { "heliocentric",    no_argument,       0, 1004 },

  // Timing Arguments
  { "date",            required_argument, 0, 1005 },
  { "time",            required_argument, 0, 1006 },
  { "timezone",        required_argument, 0, 1007 },
  { "julian-day",      required_argument, 0, 1014 },
  { "julian-calendar", no_argument,       0, 1008 },

  // Point-Selection Arguments
  { "outer",           no_argument,       0, 'o'  },
  { "asteroid",        no_argument,       0, 'a'  },
  { "uranian",         no_argument,       0, 'u'  },

  // Computational Arguments
  { "harmonic",        required_argument, 0, 1009 },
  { "dodecatemoria",   no_argument,       0, 1010 },
  { "antiscia",        no_argument,       0, 1011 },
  { "contra-antiscia", no_argument,       0, 1012 },
  { "sweph-directory", required_argument, 0, 1013 },

  // Help and Version Arguments
  { "help",            no_argument,       0, 'h'  },
  { "version",         no_argument,       0, 'v'  },

  // Mandatory last argument
  { 0, 0, 0, 0 }
};


// Main Entry-Point for the program
int main(int argc, char *argv[]) {

  // Set up to read the command-line input
  bool blValid = true;
  bool blLatCalled = false;
  bool blLonCalled = false;
  bool blAltCalled = false;
  bool blTopoCalled = false;
  bool blHelioCalled = false;
  bool blDateCalled = false;
  bool blTimeCalled = false;
  bool blZoneCalled = false;
  bool blJulianDayCalled = false;
  bool blJulianCalled = false;
  bool blHarmonicCalled = false;
  bool blDodecatemoriaCalled = false;
  bool blAntisciaCalled = false;
  bool blContraAntisciaCalled = false;
  bool blSwephCalled = false;
  bool blOuterCalled = false;
  bool blAsterCalled = false;
  bool blUraniaCalled = false;
  bool blHelpCalled = false;
  bool blVersCalled = false;
  double dLatitude, dLongitude;
  double dAltitude = 0;
  double dJulianDay;
  int nYear, nMonth, nDay, nHour, nMinute, nSecond;
  int nTzHour = 0;
  int nTzMinute = 0;
  int nTzSecond = 0;
  int nHarmonic = 1;
  char *szSwephPath;
  int nC, nOptIdx, n;

  // Read through each argument given at the command-line
  while (
    (nC = getopt_long(argc, argv, "oauhv", longopts, &nOptIdx)) != -1
  ) {
    switch (nC) {

      case 1000: // --latitude=N
        blLatCalled = true;
        dLatitude = ReadCoordinate(optarg);
        if (dLatitude < -90 || dLatitude > 90)
          blValid = false;
        break;

      case 1001: // --longitude=N
        blLonCalled = true;
        dLongitude = ReadCoordinate(optarg);
        if (dLongitude < -180 || dLongitude > 180)
          blValid = false;
        break;

      case 1002: // --altitude=N
        blAltCalled = true;
        n = sscanf(optarg, "%lf", &dAltitude);
        if (n != 1)
          blValid = false;
        break;

      case 1003: // --topocentric
        blTopoCalled = true;
        break;

      case 1004: // --heliocentric
        blHelioCalled = true;
        break;

      case 1005: // --date=N
        blDateCalled = true;
        n = ReadDate(optarg, &nYear, &nMonth, &nDay);
        if (n != 0)
          blValid = false;
        break;

      case 1006: // --time=N
        blTimeCalled = true;
        n = ReadTime(optarg, &nHour, &nMinute, &nSecond);
        if (n != 0)
          blValid = false;
        break;

      case 1007: // --timezone=N
        blZoneCalled = true;
        n = ReadTimeZone(optarg, &nTzHour, &nTzMinute, &nTzSecond);
        if (n != 0)
          blValid = false;
        break;

      case 1014: // --julian-day
        blJulianDayCalled = true;
        n = sscanf(optarg, "%lf", &dJulianDay);
        if (n != 1)
          blValid = false;
        break;

      case 1008: // --julian-calendar
        blJulianCalled = true;
        break;

      case 1009: // --harmonic=N
        blHarmonicCalled = true;
        n = sscanf(optarg, "%d", &nHarmonic);
        if (n != 1)
          blValid = false;
        if (nHarmonic < 1)
          blValid = false;
        break;

      case 1010: // --dodecatemoria
        blDodecatemoriaCalled = true;
        break;

      case 1011: // --antiscia
        blAntisciaCalled = true;
        break;

      case 1012: // --contra-antiscia
        blContraAntisciaCalled = true;
        break;

      case 1013: // --sweph-directory=N
        blSwephCalled = true;
        szSwephPath = (char*)malloc(
          sizeof(char) * (strlen(optarg) + 1)
        );
        strcpy(szSwephPath, optarg);
        break;

      case 'o': // --outer / -o
        blOuterCalled = true;
        break;

      case 'a': // --asteroid / -a
        blAsterCalled = true;
        break;

      case 'u': // --uranian / -u
        blUraniaCalled = true;
        break;

      case 'h': // --help / -h
        blHelpCalled = true;
        break;

      case 'v': // --version / -v
        blVersCalled = true;
        break;

      default: // INVALID OPTION
        blValid = false;
        break;
    }
  }

  // If help and/or version were called, display it/them and stop here
  if (blVersCalled) {
    ShowVersion();
  }
  if (blHelpCalled) {
    ShowHelp();
  }
  if (blVersCalled || blHelpCalled) {
    return 0;
  }

  // Attempt to invalidate the arguments combination
  //
  // Date and Time, both, must be called, or else only Julian Day. If
  // Julian Day is called, then timezone may not be
  if (
    blJulianDayCalled &&
    (blDateCalled || blTimeCalled || blZoneCalled)
  )
    blValid = false;
  if (!blJulianDayCalled) {
    if (!blDateCalled || !blTimeCalled)
      blValid = false;
  }
  //
  // If heliocentric calculation, then cannot have earth-based position,
  // otherwise earth-position is required
  if (blHelioCalled) {
    if (blLatCalled || blLonCalled || blAltCalled)
      blValid = false;
  } else {
    if (!blLatCalled || !blLonCalled) // altitude default to zero
      blValid = false;
  }
  //
  // Check to see if attempting harmonics AND a dodecatemoria chart BOTH
  // (unless Harmonic-1 has been explicitly called)
  if (blHarmonicCalled && blDodecatemoriaCalled) {
    if (nHarmonic != 1)
      blValid = false;
  }
  //
  // Check to see if attempting harmonics AND a (contra)antiscia chart
  // BOTH (unless Harmonic-1 has been explicitly called)
  if (
    blHarmonicCalled &&
    (blAntisciaCalled || blContraAntisciaCalled)
  ) {
    if (nHarmonic != 1)
      blValid = false;
  }
  //
  // Check to see if attempting (contra)antiscia and dodecatemoria BOTH
  if (
    blDodecatemoriaCalled &&
    (blAntisciaCalled || blContraAntisciaCalled)
  ) {
    blValid = false;
  }

  // If the input was invalidated, display the help screen and stop here
  if (!blValid) {
    ShowHelp();
    return 1;
  }

  // Otherwise we may proceed
  int nFlags;

  // If the Sweph Path was specified, load it here
  if (blSwephCalled) {
    swe_set_ephe_path(szSwephPath);
    nFlags = SEFLG_SWIEPH;
  } else {
    swe_set_ephe_path(NULL);
    nFlags = SEFLG_MOSEPH;
  }
  nFlags |= SEFLG_SPEED;

  // Create the DateTime instance using either the Julian Day or
  // date-time and time-zone provided
  DateTime lTime;
  if (blJulianDayCalled) {
    // Using Julian Day
    // Process the date-time from the Julian Day directly
    lTime = DateTime(dJulianDay);
  } else {
    // Not using Julian Days
    // Process the date-time from date and time and time-zone
    double dHour =
      (double)nHour + ((double)nMinute / 60) + ((double)nSecond / 3600);
    bool blJulian = false;
    if (nYear < 1582)
      blJulian = true;
    else if (nYear == 1582) {
      if (nMonth < 10)
        blJulian = true;
      else if (nMonth == 10) {
        if (nDay < 15)
          blJulian = true;
      }
    }
    if (blJulianCalled)
      blJulian = true;
    if (blJulian) {
      lTime = DateTime::FromJulian(nYear, nMonth, nDay, dHour);
    } else {
      lTime = DateTime::FromGregorian(nYear, nMonth, nDay, dHour);
    }
    // Apply the time-zone adjustment to this DateTime
    lTime.ApplyTimeZone(nTzHour, nTzMinute, nTzSecond);
  }

  // Which computation has been requested?
  Chart *plChart;
  GeographicLocation sLocation;
  sLocation.Latitude = dLatitude;
  sLocation.Longitude = dLongitude;
  sLocation.Altitude = dAltitude;
  if (blHelioCalled) {
    plChart = new HelioChart(&lTime);
    nFlags |= SEFLG_HELCTR;
  } else {
    plChart = new GeoChart(&lTime, sLocation);
    if (blTopoCalled) {
      ((GeoChart*)plChart)->SetTopocentric(true);
      nFlags |= SEFLG_TOPOCTR;
    }
  }
  if (blOuterCalled)
    plChart->SetOuters(true);
  if (blAsterCalled)
    plChart->SetAsteroids(true);
  if (blUraniaCalled)
    plChart->SetUranians(true);

  // Apply the computational flags, harmonics, and specify whether chart
  // is dodecatemoria
  plChart->SetComputationFlags(nFlags);
  plChart->SetHarmonic(nHarmonic);
  plChart->SetDodecatemoria(blDodecatemoriaCalled);
  plChart->SetAntiscia(blAntisciaCalled);
  plChart->SetContraAntiscia(blContraAntisciaCalled);

  // Compute the chart and display it
  plChart->Compute();
  plChart->PrintChart();

  // Return to the system
  return 0;
}


// Function to show program version info
void ShowVersion() {
  printf("%s\n", PACKAGE_STRING);
  printf("Copyright © 2020  Daniel McLaughlin\n");
}

// Function to display program help
void ShowHelp() {
  printf("Usage: %s --date=DATE --time=TIME [other arguments]\n", PACKAGE);
  printf("\n");
  printf("Location Arguments\n");
  printf("Charts may be cast from either geocentric or heliocentric vantage\n");
  printf("points, and the geocentric vantage may either be truly geocentric or\n");
  printf("topocentric.\n");
  printf("  --latitude=LATITUDE\n");
  printf("  --longitude=LONGITUDE\n");
  printf("     If casting from a geocentric vantage (option --heliocentric is not\n");
  printf("     used), both of these arguments are required.\n");
  printf("     Both coordinates are entered in one of the formats:\n");
  printf("       D.DDD\n");
  printf("       zD.DDD\n");
  printf("       DzMM.MMM\n");
  printf("        Examples:\n");
  printf("         -45.27  - forty-five point two seven degrees either west or\n");
  printf("                   south\n");
  printf("         n32.85  - thirty-two point eight five degrees north\n");
  printf("         24w31.4 - twenty-four degrees and thirty-one point four minutes\n");
  printf("                   west\n");
  printf("       Note that positive values are interpreted as north or east and\n");
  printf("       negative values are interpreted as south or west.\n");
  printf("\n");
  printf("  --altitude=ALT\n");
  printf("     This optional argument specifies the altitude of the location above\n");
  printf("     sea-level; the altitude is given in meters. This argument is only\n");
  printf("     meaningful if --topocentric has been called.\n");
  printf("\n");
  printf("  --topocentric\n");
  printf("  --heliocentric\n");
  printf("     Specifies either a topocentric or a heliocentric vantage. Both\n");
  printf("     arguments may not be called at the same time. If neither is called,\n");
  printf("     then the chart returned is truly geocentric.\n");
  printf("\n");
  printf("  If the option --heliocentric has been called, then the latitude,\n");
  printf("  longitude, and altitude may not be called; if --heliocentric has been\n");
  printf("  called, then the latitude and longitude, both, are required. Altitude\n");
  printf("  may be used without --heliocentric, but is not required.\n");
  printf("\n");
  printf("Time Arguments\n");
  printf("  --date=YYYY-MM-DD\n");
  printf("  --time=HH:mm(:ss)\n");
  printf("     Specify the date and time. Both of these arguments are ALWAYS\n");
  printf("     required every time AstroStar is called.\n");
  printf("\n");
  printf("  --timezone=TZ\n");
  printf("     Specify the time-zone on the given date-time. If omitted, the given\n");
  printf("     date-time will be interpreted on UTC time.\n");
  printf("     The timezone may be entered in one of two formats:\n");
  printf("       hNz(Nm) - hour-time\n");
  printf("       mNzN.NN - meridian-time\n");
  printf("        Examples:\n");
  printf("         h4w30  - four hours and thirty minutes west of Greenwich\n");
  printf("         m45e30 - fourty-five degrees and thirty minutes east of\n");
  printf("            Greenwich (corresponding to three hours and two minutes east\n");
  printf("            of Greenwich)\n");
  printf("\n");
  printf("  --julian-day=JD\n");
  printf("     Specify the date-time in Julian Days\n");
  printf("\n");
  printf("  Must use either --date and --time, together, or use --julian-day on\n");
  printf("  its own. You cannot use --julian-day with either --date or --time.\n");
  printf("  The argument --timezone may only be used with --date and --time, not\n");
  printf("  with --julian-day.\n");
  printf("\n");
  printf("  --julian-calendar\n");
  printf("     All dates entered prior to 15 October 1582 are interpreted on the\n");
  printf("     Julian Calendar and all dates after on the Gregorian Calendar. This\n");
  printf("     switch will override the Gregorian Calendar and interpret the date\n");
  printf("     on the Julian Calendar.\n");
  printf("\n");
  printf("Computational Arguments\n");
  printf("  --harmonic=N\n");
  printf("     Specify the harmonic of the chart. If omitted, then the chart will\n");
  printf("     return the positions in the first harmonic (the actual positions).\n");
  printf("     The harmonic must be a positive, whole number.\n");
  printf("\n");
  printf("  --dodecatemoria\n");
  printf("     This option specifies that dodecatemoria positions are to be\n");
  printf("     returned rather than the actual positions.\n");
  printf("  --antiscia\n");
  printf("     This option specifies that antiscia positions are to be returned\n");
  printf("     rather than the actual positions.\n");
  printf("  --contra-antiscia\n");
  printf("     This option specifies that contra-antiscia positions are to be\n");
  printf("     returned rather than the actual positions.\n");
  printf("\n");
  printf("  Note that the --harmonic, --dodecatemoria, and either --antiscia or\n");
  printf("  --contra-antiscia arguments may NOT be used in any combination at the\n");
  printf("  same time. However --antiscia and --contra-antiscia may both be used\n");
  printf("  together.\n");
  printf("\n");
  printf("Planet Selection Arguments\n");
  printf("  -o, --outer\n");
  printf("     Include the outer planets (Uranus, Neptune, and Pluto) in the\n");
  printf("     chart.\n");
  printf("  -a, --asteroid\n");
  printf("     Include the major asteroids (Ceres, Pallas, Juno, Vesta, Pholus,\n");
  printf("     and Chiron) in the chart.\n");
  printf("  -u, --uranian\n");
  printf("     Include the Uranian planets (Cupido, Hades, Zeus, Kronos, Apollon,\n");
  printf("     Admetos, Vulcanus, and Poseidon, from the Hamburg School) in the\n");
  printf("     chart.\n");
  printf("\n");
  printf("Other Arguments\n");
  printf("  --sweph-directory=DIR\n");
  printf("     This option specifies the location of the Swiss Ephemeris files in\n");
  printf("     the system. If this option is used, then the Swiss Ephemeris will\n");
  printf("     be used, if omitted, then the Moshier Ephemeris will be used\n");
  printf("     instead.\n");
  printf("\n");
  printf("  -h, --help\n");
  printf("     Show this help.\n");
  printf("  -v, --version\n");
  printf("     Show the version information.\n");
  printf("\n");
}

