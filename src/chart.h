/* chart.h
 * Copyright © 2020 Daniel McLaughlin
 *
 * The file is part of the Ceastar application.
 *
 * Ceastar is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Ceastar is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * Ceastar. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ___CHART_H__
#define ___CHART_H__

// Include the application header
#include "ceastar.h"

// Include the required class-headers
#include "datetime.h"
#include "point.h"

// Include the required system headers
#include <vector>

// Base-Class representing astrological charts
class Chart {
  protected:

    // Point-vectors for the traditional and outer planets, the
    // asteroids, and the Uranian (Hamburger) planets
    std::vector<ChartPoint> m_vPlanets;
    std::vector<ChartPoint> m_vOuters;
    std::vector<ChartPoint> m_vAsteroids;
    std::vector<ChartPoint> m_vUranians;

    // DateTime holding the time of the chart
    DateTime *m_plTime;

    // True/False values indicating whether to display outer planets,
    // asteroids, and Uranian planets
    bool m_blOuters, m_blAsteroids, m_blUranians;

    // Values indicating whether to compute a dodecatemoria chart, a
    // (contra)antiscia chart, or a harmonic chart
    bool m_blDodecatemoria;
    bool m_blAntiscia, m_blContraAntiscia;
    int m_nHarmonic;

    // Value holding the computational flags for SWEPH
    int m_nComputeFlags;

    // Non-Public default constructor
    Chart();

  public:

    // Destructor
    ~Chart();

    // Functions to set whether to display the outer planets, asteroids,
    // and Uranian (Hamburger) planets
    void SetOuters(
      const bool blOuters
    );
    void SetAsteroids(
      const bool blAsteroids
    );
    void SetUranians(
      const bool blUranians
    );
    /* For all: True for yes to display, False for no */

    // Functions to get or set whether computing dodecatemoria
    bool GetDodecatemoria(
      void
    ) const;
    void SetDodecatemoria(
      const bool blDodecatemoria
        /* True for dodecatemoria, False for regular */
    );

    // Functions to get or set whether computing (contra)antiscia
    bool GetAntiscia(void) const;
    bool GetContraAntiscia(void) const;
    void SetAntiscia(const bool blAntiscia);
    void SetContraAntiscia(const bool blContraAntiscia);

    // Functions to get or set the chart harmonic
    int GetHarmonic(
      void
    ) const;
    void SetHarmonic(
      const int nHarmonic
        /* Harmonic of the chart; must be 1 or greater */
    );

    // Functions to get or set the computational flags for SWEPH
    int GetComputationFlags(
      void
    ) const;
    void SetComputationFlags(
      const int nFlags
        /* Computational flags for SWEPH */
    );

    // Pure-Virtual functions to compute the chart and display it
    virtual void Compute(
      void
    ) = 0;
    virtual void PrintChart(
      void
    ) = 0;
    // MUST be implemented in daughter-classes !!

    // Function to extract the date and hour of the chart from its time
    void GetDate(
      const bool blGregorian,
        /* True to output a Gregorian date, */
        /* False to output a Julian Date    */
      int *pnYear,
      int *pnMonth,
      int *pnDay,
      double *pdHour
        /* Integer and Double-Float pointers to */
        /* hold the date and hour on return     */
    ) const;
};

// Class representing a geocentric astrological chart
class GeoChart : public Chart {
  private:

    // Point-vectors for the luminaries, angles, and lots
    std::vector<ChartPoint> m_vLuminaries;
    std::vector<ChartPoint> m_vAngles;
    std::vector<ChartPoint> m_vParts;

    // Values to hold the mean and true axial obliquity at the time of
    // the chart
    double m_dTrueObliq, m_dMeanObliq;

    // Values to hold the altitude-angle and azimuth of the ...
    double m_dAltEcliptMax, m_dAziEcliptMax;

    // Location to hold the geographical location of the chart
    GeographicLocation m_sLocation;

    // Boolean value to hold whether chart is truly geocentric, or
    // topocentric
    bool m_blTopocentric;

    // Non-Public default constructor
    GeoChart();

  public:

    // Destructor
    ~GeoChart();

    // Constructor
    GeoChart(
      DateTime *plTime,
        /* DateTime instance with the time of the chart */
      GeographicLocation sLocation
        /* Geographical location of the chart */
    );

    // Functions to get or set whether chart is topocentric
    bool GetTopocentric(
      void
    ) const;
    void SetTopocentric(
      const bool blTopocentric
        /* True for topocentric, False for geocentric */
    );

    // Function to compute the chart
    void Compute(
      void
    );

    // Function to display the chart
    void PrintChart(
      void
    );
};

// Class representing a heliocentric astrological chart
class HelioChart : public Chart {
  private:

    // Non-Public default constructor
    HelioChart();

  public:

    // Destructor
    ~HelioChart();

    // Constructor
    HelioChart(
      DateTime *plTime
        /* DateTime instance for the time of the chart */
    );

    // Function to compute all of the points in the chart
    void Compute(
      void
    );

    // Function to display the chart
    void PrintChart(
      void
    );
};

#endif // ___CHART_H__

