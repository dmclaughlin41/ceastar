/* chart.cpp
 * Copyright © 2020 Daniel McLaughlin
 *
 * The file is part of the Ceastar application.
 *
 * Ceastar is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Ceastar is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * Ceastar. If not, see <https://www.gnu.org/licenses/>.
 */

// Include the application-header
#include "ceastar.h"

// Include the required system-headers
#include <vector>
#include <swephexp.h>

// Use the "std" namespace for the vectors
using namespace std;



// Default constructor and destructor for the Chart-class
Chart::Chart() {

  // Initialize the outer planets
  this->m_vOuters = vector<ChartPoint>();
  this->m_vOuters.push_back(ChartPoint(SE_URANUS));
  this->m_vOuters.push_back(ChartPoint(SE_NEPTUNE));
  this->m_vOuters.push_back(ChartPoint(SE_PLUTO));

  // Initialize the major asteroids
  this->m_vAsteroids = vector<ChartPoint>();
  this->m_vAsteroids.push_back(ChartPoint(SE_CERES));
  this->m_vAsteroids.push_back(ChartPoint(SE_PALLAS));
  this->m_vAsteroids.push_back(ChartPoint(SE_JUNO));
  this->m_vAsteroids.push_back(ChartPoint(SE_VESTA));
  this->m_vAsteroids.push_back(ChartPoint(SE_PHOLUS));
  this->m_vAsteroids.push_back(ChartPoint(SE_CHIRON));

  // Initialize the Uranian planets
  this->m_vUranians = vector<ChartPoint>();
  this->m_vUranians.push_back(ChartPoint(SE_CUPIDO));
  this->m_vUranians.push_back(ChartPoint(SE_HADES));
  this->m_vUranians.push_back(ChartPoint(SE_ZEUS));
  this->m_vUranians.push_back(ChartPoint(SE_KRONOS));
  this->m_vUranians.push_back(ChartPoint(SE_APOLLON));
  this->m_vUranians.push_back(ChartPoint(SE_ADMETOS));
  this->m_vUranians.push_back(ChartPoint(SE_VULKANUS));
  this->m_vUranians.push_back(ChartPoint(SE_POSEIDON));

  // Initialize the display to traditional planets only with default
  // computation flags; initialize the date-time instance to JD:0 and
  // set the harmonic to the default of one
  this->m_blOuters = false;
  this->m_blAsteroids = false;
  this->m_blUranians = false;
  this->m_blDodecatemoria = false;
  this->m_nComputeFlags = SEFLG_SWIEPH | SEFLG_SPEED;
  this->m_nHarmonic = 1;
  this->m_plTime = new DateTime(0);
}
Chart::~Chart() {

  // Clear the planet/point vector-arrays and zeroize the computation
  // flags
  this->m_vPlanets.clear();
  this->m_vOuters.clear();
  this->m_vAsteroids.clear();
  this->m_vUranians.clear();
  this->m_nComputeFlags = 0;
}

// Functions to Set Point Selections
void Chart::SetOuters(const bool blOuters) {
  this->m_blOuters = blOuters;
}
void Chart::SetAsteroids(const bool blAsteroids) {
  this->m_blAsteroids = blAsteroids;
}
void Chart::SetUranians(const bool blUranians) {
  this->m_blUranians = blUranians;
}

// Functions to get and set dodecatemoria computation
bool Chart::GetDodecatemoria(void) const {
  return this->m_blDodecatemoria;
}
void Chart::SetDodecatemoria(const bool blDodecatemoria) {
  this->m_blDodecatemoria = blDodecatemoria;
}

// Functions to get and set (contra)antiscia computation
bool Chart::GetAntiscia() const {
  return this->m_blAntiscia;
}
bool Chart::GetContraAntiscia() const {
  return this->m_blContraAntiscia;
}
void Chart::SetAntiscia(const bool blAntiscia) {
  this->m_blAntiscia = blAntiscia;
}
void Chart::SetContraAntiscia(const bool blContraAntiscia) {
  this->m_blContraAntiscia = blContraAntiscia;
}

// Functions to get and set the chart harmonic
int Chart::GetHarmonic(void) const {
  return this->m_nHarmonic;
}
void Chart::SetHarmonic(const int nHarmonic) {
  this->m_nHarmonic = nHarmonic;
}

// Functions to get and set the computation flags
int Chart::GetComputationFlags(void) const {
  return this->m_nComputeFlags;
}
void Chart::SetComputationFlags(const int nFlags) {
  this->m_nComputeFlags = nFlags;
}

// Function to get the date from the chart
void Chart::GetDate(const bool blGregorian, int *pnYear, int *pnMonth,
                                     int *pnDay, double *pdHour) const {
  this->m_plTime->GetDate(pnYear, pnMonth, pnDay, pdHour, blGregorian);
}



// Default constructor and destructor for the GeoChart-class
GeoChart::GeoChart() : Chart() {

  // Set the latitude, longitude and altitude to zero; set for default
  // geocentric computation
  this->m_sLocation.Latitude = (double)0;
  this->m_sLocation.Longitude = (double)0;
  this->m_sLocation.Altitude = (double)0;
  this->m_blTopocentric = false;

  // Initialize the traditional planets (excluding luminaries)
  this->m_vPlanets = vector<ChartPoint>();
  this->m_vPlanets.push_back(ChartPoint(SE_MERCURY));
  this->m_vPlanets.push_back(ChartPoint(SE_VENUS));
  this->m_vPlanets.push_back(ChartPoint(SE_MARS));
  this->m_vPlanets.push_back(ChartPoint(SE_JUPITER));
  this->m_vPlanets.push_back(ChartPoint(SE_SATURN));

  // Initialize the luminaries (include the lunar node in this list)
  this->m_vLuminaries = vector<ChartPoint>();
  this->m_vLuminaries.push_back(ChartPoint(SE_SUN));
  this->m_vLuminaries.push_back(ChartPoint(SE_MOON));
  this->m_vLuminaries.push_back(ChartPoint(SE_TRUE_NODE));

  // Initialize the angle-vector
  this->m_vAngles = vector<ChartPoint>();

  // Initialize the parts (lots)
  this->m_vParts = vector<ChartPoint>();

  // Set the Obliquity and Maximum values to default zero
  this->m_dTrueObliq = (double)0;
  this->m_dMeanObliq = (double)1;
  this->m_dAltEcliptMax = (double)0;
  this->m_dAziEcliptMax = (double)0;
}
GeoChart::~GeoChart() {

  // Clear the planet/point vector-arrays
  this->m_vLuminaries.clear();
  this->m_vAngles.clear();
  this->m_vParts.clear();

  // Zeroize the location latitude, longitude, and altitude
  this->m_sLocation.Latitude = (double)0;
  this->m_sLocation.Longitude = (double)0;
  this->m_sLocation.Altitude = (double)0;

  // Set to false the topocentric indicator
  this->m_blTopocentric = false;
}

// Primary constructor for the GeoChart-class with time and location
// provided
GeoChart::GeoChart(DateTime *plTime, GeographicLocation sLocation) :
                                                            GeoChart() {
  // Set the time and location to the parameter-values
  this->m_sLocation.Latitude = sLocation.Latitude;
  this->m_sLocation.Longitude = sLocation.Longitude;
  this->m_sLocation.Altitude = sLocation.Altitude;
  this->m_plTime->SetTime(plTime->GetTime());
}

// Functions to get and set the topocentric-computation indicator
bool GeoChart::GetTopocentric(void) const {
  return this->m_blTopocentric;
}
void GeoChart::SetTopocentric(bool blTopocentric) {
  this->m_blTopocentric = blTopocentric;
}

// Function to perform the computations on all points in the chart
void GeoChart::Compute(void) {
  double dVal;

  // Get the obliquity of the ecliptic at the moment of the chart
  double dVals[6];
  char szErr[256];
  swe_calc(this->m_plTime->GetTime(), SE_ECL_NUT, 0, dVals, szErr);
  this->m_dTrueObliq = dVals[0];
  this->m_dMeanObliq = dVals[1];

  // Go through all of the luminaries and calculate them
  for (int i = 0; i < this->m_vLuminaries.size(); i++) {
    ((ChartPoint&)this->m_vLuminaries[i]).Compute(
      this->m_plTime->GetTime(),
      this->m_sLocation,
      this->m_nComputeFlags
    );
    dVal = this->m_vLuminaries[i].Longitude;
    dVal *= this->m_nHarmonic;
    while (dVal >= 360) { dVal -= 360; }
    this->m_vLuminaries[i].Longitude = dVal;
    this->m_vLuminaries[i].LongitudeSpeed *= this->m_nHarmonic;
    if (this->m_blDodecatemoria) {
      this->m_vLuminaries[i].Longitude =
        ChartPoint::DodecatemoriaDegree(
          this->m_vLuminaries[i].Longitude
        );
      this->m_vLuminaries[i].LongitudeSpeed *= 12;
    }
    if (this->m_blAntiscia) {
      this->m_vLuminaries[i].Longitude =
        ChartPoint::AntisciaDegree(
          this->m_vLuminaries[i].Longitude
        );
      this->m_vLuminaries[i].LongitudeSpeed *= -1;
    }
    if (this->m_blContraAntiscia) {
      this->m_vLuminaries[i].Longitude =
        ChartPoint::ContraAntisciaDegree(
          this->m_vLuminaries[i].Longitude
        );
      this->m_vLuminaries[i].LongitudeSpeed *= -1;
    }
  }

  // Go through all of the traditional planets and calculate them
  for (int i = 0; i < this->m_vPlanets.size(); i++) {
    ((ChartPoint&)this->m_vPlanets[i]).Compute(
      this->m_plTime->GetTime(),
      this->m_sLocation,
      this->m_nComputeFlags
    );
    dVal = this->m_vPlanets[i].Longitude;
    dVal *= this->m_nHarmonic;
    while (dVal >= 360) { dVal -= 360; }
    this->m_vPlanets[i].Longitude = dVal;
    this->m_vPlanets[i].LongitudeSpeed *= this->m_nHarmonic;
    if (this->m_blDodecatemoria) {
      this->m_vPlanets[i].Longitude =
        ChartPoint::DodecatemoriaDegree(
          this->m_vPlanets[i].Longitude
        );
      this->m_vPlanets[i].LongitudeSpeed *= 12;
    }
    if (this->m_blAntiscia) {
      this->m_vPlanets[i].Longitude =
        ChartPoint::AntisciaDegree(
          this->m_vPlanets[i].Longitude
        );
      this->m_vPlanets[i].LongitudeSpeed *= -1;
    }
    if (this->m_blContraAntiscia) {
      this->m_vPlanets[i].Longitude =
        ChartPoint::ContraAntisciaDegree(
          this->m_vPlanets[i].Longitude
        );
      this->m_vPlanets[i].LongitudeSpeed *= -1;
    }
  }

  // Go through all of the outer planets and calculate them
  for (int i = 0; i < this->m_vOuters.size(); i++) {
    ((ChartPoint&)this->m_vOuters[i]).Compute(
      this->m_plTime->GetTime(),
      this->m_sLocation,
      this->m_nComputeFlags
    );
    dVal = this->m_vOuters[i].Longitude;
    dVal *= this->m_nHarmonic;
    while (dVal >= 360) { dVal -= 360; }
    this->m_vOuters[i].Longitude = dVal;
    this->m_vOuters[i].LongitudeSpeed *= this->m_nHarmonic;
    if (this->m_blDodecatemoria) {
      this->m_vOuters[i].Longitude =
        ChartPoint::DodecatemoriaDegree(
          this->m_vOuters[i].Longitude
        );
      this->m_vOuters[i].LongitudeSpeed *= 12;
    }
    if (this->m_blAntiscia) {
      this->m_vOuters[i].Longitude =
        ChartPoint::AntisciaDegree(
          this->m_vOuters[i].Longitude
        );
      this->m_vOuters[i].LongitudeSpeed *= -1;
    }
    if (this->m_blContraAntiscia) {
      this->m_vOuters[i].Longitude =
        ChartPoint::ContraAntisciaDegree(
          this->m_vOuters[i].Longitude
        );
      this->m_vOuters[i].LongitudeSpeed *= -1;
    }
  }

  // Go through all of the asteroids and calculate them
  for (int i = 0; i < this->m_vAsteroids.size(); i++) {
    ((ChartPoint&)this->m_vAsteroids[i]).Compute(
      this->m_plTime->GetTime(),
      this->m_sLocation,
      this->m_nComputeFlags
    );
    dVal = this->m_vAsteroids[i].Longitude;
    dVal *= this->m_nHarmonic;
    while (dVal >= 360) { dVal -= 360; }
    this->m_vAsteroids[i].Longitude = dVal;
    this->m_vAsteroids[i].LongitudeSpeed *= this->m_nHarmonic;
    if (this->m_blDodecatemoria) {
      this->m_vAsteroids[i].Longitude =
        ChartPoint::DodecatemoriaDegree(
          this->m_vAsteroids[i].Longitude
        );
      this->m_vAsteroids[i].LongitudeSpeed *= 12;
    }
    if (this->m_blAntiscia) {
      this->m_vAsteroids[i].Longitude =
        ChartPoint::AntisciaDegree(
          this->m_vAsteroids[i].Longitude
        );
      this->m_vAsteroids[i].LongitudeSpeed *= -1;
    }
    if (this->m_blContraAntiscia) {
      this->m_vAsteroids[i].Longitude =
        ChartPoint::ContraAntisciaDegree(
          this->m_vAsteroids[i].Longitude
        );
      this->m_vAsteroids[i].LongitudeSpeed *= -1;
    }
  }

  // Go through all of the uranian planets and calculate them
  for (int i = 0; i < this->m_vUranians.size(); i++) {
    ((ChartPoint&)this->m_vUranians[i]).Compute(
      this->m_plTime->GetTime(),
      this->m_sLocation,
      this->m_nComputeFlags
    );
    dVal = this->m_vUranians[i].Longitude;
    dVal *= this->m_nHarmonic;
    while (dVal >= 360) { dVal -= 360; }
    this->m_vUranians[i].Longitude = dVal;
    this->m_vUranians[i].LongitudeSpeed *= this->m_nHarmonic;
    if (this->m_blDodecatemoria) {
      this->m_vUranians[i].Longitude =
        ChartPoint::DodecatemoriaDegree(
          this->m_vUranians[i].Longitude
        );
      this->m_vUranians[i].LongitudeSpeed *= 12;
    }
    if (this->m_blAntiscia) {
      this->m_vUranians[i].Longitude =
        ChartPoint::AntisciaDegree(
          this->m_vUranians[i].Longitude
        );
      this->m_vUranians[i].LongitudeSpeed *= -1;
    }
    if (this->m_blContraAntiscia) {
      this->m_vUranians[i].Longitude =
        ChartPoint::ContraAntisciaDegree(
          this->m_vUranians[i].Longitude
        );
      this->m_vUranians[i].LongitudeSpeed *= -1;
    }
  }

  // Compute the Medium Coeli, Ascendant, and Vertex
  double adCusp[13];
  double adAngle[10];
  swe_houses(
    this->m_plTime->GetTime(), this->m_sLocation.Latitude,
    this->m_sLocation.Longitude, 'P', adCusp, adAngle
  );

  // Compute the ecliptic longitude of the ecliptic maxium from the
  // ascendant (10th house cusp of equal house); compute the horizontal
  // azimuth and elevation of that point on the ecliptic
  double adGeo[3];
  adGeo[0] = this->m_sLocation.Longitude;
  adGeo[1] = this->m_sLocation.Latitude;
  adGeo[2] = this->m_sLocation.Altitude;
  double dLonEcliptMax = adAngle[0] - 90;
  if (dLonEcliptMax < 0)
    dLonEcliptMax += 360;
  double adEclipt[3];
  double adHoriz[3];
  adEclipt[0] = dLonEcliptMax;
  adEclipt[1] = 0;
  adEclipt[2] = 0;
  swe_azalt(
    this->m_plTime->GetTime(), SE_ECL2HOR,
    adGeo, 1013.25, 22.4, adEclipt, adHoriz
  );
  this->m_dAltEcliptMax = adHoriz[1];
  double dAziEclMax = adHoriz[0] + 180;
  while (dAziEclMax < 0) { dAziEclMax += 360; }
  while (dAziEclMax >= 360) { dAziEclMax -= 360; }
  this->m_dAziEcliptMax = dAziEclMax;

  // Load the Medium Coeli, Ascendant, and Vertex ... accounting for
  // harmonics and dodecatemoria
  double dAsc = adAngle[0];
  double dMc  = adAngle[1];
  double dVx  = adAngle[3];
  double dDecAsc =
    ChartPoint::ZodiacDeclination(dAsc, this->m_dTrueObliq);
  double dDecMc  =
    ChartPoint::ZodiacDeclination(dMc,  this->m_dTrueObliq);
  double dDecVx  =
    ChartPoint::ZodiacDeclination(dVx,  this->m_dTrueObliq);
  dAsc *= this->m_nHarmonic;
  while (dAsc >= 360) { dAsc -= 360; }
  dMc *= this->m_nHarmonic;
  while (dMc >=  360) { dMc  -= 360; }
  dVx *= this->m_nHarmonic;
  while (dVx >=  360) { dVx  -= 360; }
  if (this->m_blDodecatemoria) {
    dAsc = ChartPoint::DodecatemoriaDegree(dAsc);
    dMc  = ChartPoint::DodecatemoriaDegree(dMc);
    dVx  = ChartPoint::DodecatemoriaDegree(dVx);
  }
  if (this->m_blAntiscia) {
    dAsc = ChartPoint::AntisciaDegree(dAsc);
    dMc  = ChartPoint::AntisciaDegree(dMc);
    dVx  = ChartPoint::AntisciaDegree(dVx);
  }
  if (this->m_blContraAntiscia) {
    dAsc = ChartPoint::ContraAntisciaDegree(dAsc);
    dMc  = ChartPoint::ContraAntisciaDegree(dMc);
    dVx  = ChartPoint::ContraAntisciaDegree(dVx);
  }
  this->m_vAngles.clear();
  this->m_vAngles.push_back(
    ChartPoint("Ascendant", dAsc, dDecAsc)
  );
  this->m_vAngles.push_back(
    ChartPoint("MC", dMc, dDecMc)
  );
  this->m_vAngles.push_back(
    ChartPoint("Vertex", dVx, dDecVx)
  );

  // Recalculate the actual positions (not harmonic, (contra)antiscia,
  // or dodecatemorian) for the Sun, Moon, and Ascendant--to be used for
  // computing the Lots of Fortune and Spirit
  swe_calc_ut(
    this->m_plTime->GetTime(), SE_SUN,
    this->m_nComputeFlags, dVals, szErr
  );
  double dSun = dVals[0];
  swe_calc_ut(
    this->m_plTime->GetTime(), SE_MOON,
    this->m_nComputeFlags, dVals, szErr
  );
  double dMoon = dVals[0];
  swe_houses(
    this->m_plTime->GetTime(), this->m_sLocation.Latitude,
    this->m_sLocation.Longitude, 'P', adCusp, adAngle
  );
  dAsc = adAngle[0];
  double dDsc = dAsc + 180;
  while (dDsc >= 360) { dDsc -= 360; }

  // Is the chart-sect day or night?
  bool blNight = false;
  if (dAsc == 180) {
    if (dSun > 180)
      blNight = true;
  }
  else if (dAsc == 0) {
    if (dSun > 0 && dSun < 180)
      blNight = true;
  }
  else if (dAsc < 180) {
    if (dSun > dAsc && dSun < dDsc)
      blNight = true;
  }
  else { // dAsc > 180
    if (dSun > dAsc || dSun < dDsc)
      blNight = true;
  }

  // Compute the Lots of Fortune and Spirit
  double dFort, dSpir;
  if (blNight) {
    dFort = dAsc + dSun - dMoon;
    dSpir = dAsc + dMoon - dSun;
  } else {
    dFort = dAsc + dMoon - dSun;
    dSpir = dAsc + dSun - dMoon;
  }
  while (dFort  <   0) { dFort += 360; }
  while (dFort >= 360) { dFort -= 360; }
  while (dSpir  <   0) { dSpir += 360; }
  while (dSpir >= 360) { dSpir -= 360; }
  double dDecFort =
    ChartPoint::ZodiacDeclination(dFort, this->m_dTrueObliq);
  double dDecSpir =
    ChartPoint::ZodiacDeclination(dSpir, this->m_dTrueObliq);

  // With the actual positions of Spirit and Fortune now had, apply any
  // harmonics, (contra)antiscia, or dodecatemoria
  dFort *= this->m_nHarmonic;
  dSpir *= this->m_nHarmonic;
  while (dFort  <   0) { dFort += 360; }
  while (dFort >= 360) { dFort -= 360; }
  while (dSpir  <   0) { dSpir += 360; }
  while (dSpir >= 360) { dSpir -= 360; }
  if (this->m_blDodecatemoria) {
    dFort = ChartPoint::DodecatemoriaDegree(dFort);
    dSpir = ChartPoint::DodecatemoriaDegree(dSpir);
  }
  if (this->m_blAntiscia) {
    dFort  = ChartPoint::AntisciaDegree(dFort);
    dSpir  = ChartPoint::AntisciaDegree(dSpir);
  }
  if (this->m_blContraAntiscia) {
    dFort  = ChartPoint::ContraAntisciaDegree(dFort);
    dSpir  = ChartPoint::ContraAntisciaDegree(dSpir);
  }

  // Load the Lots of Fortune and Spirit to the Parts-vector
  this->m_vParts.clear();
  this->m_vParts.push_back(ChartPoint("Fortuna", dFort, dDecFort));
  this->m_vParts.push_back(ChartPoint("Spiritus", dSpir, dDecSpir));
}

// Function to print out the chart
void GeoChart::PrintChart(void) {

  // Print the location and time information for the chart
  printf(" Latitude: %lf\n", this->m_sLocation.Latitude);
  printf("Longitude: %lf\n", this->m_sLocation.Longitude);
  printf(" Altitude: %lf\n", this->m_sLocation.Altitude);
  printf("       JD: %lf\n", this->m_plTime->GetTime());

  // Allocate four strings to hold formatted degrees
  // Declare the boolean value to hold whether retrograde
  char *szDegree, *szDegreeSpeed, *szDeclin, *szDeclinSpeed;
  szDegree      = (char*)malloc(sizeof(char) * 128);
  szDegreeSpeed = (char*)malloc(sizeof(char) * 128);
  szDeclin      = (char*)malloc(sizeof(char) * 128);
  szDeclinSpeed = (char*)malloc(sizeof(char) * 128);
  bool blRetro;

  // Print the obliquity for the chart
  WritePreciseDegree(szDegree, this->m_dMeanObliq);
  printf("Mean Obliquity: %s\n", szDegree);
  WritePreciseDegree(szDegree, this->m_dTrueObliq);
  printf("True Obliquity: %s\n", szDegree);

  // Print the altitude and azimuth of the ecliptical maximum
  WriteDegree(szDeclin, this->m_dAltEcliptMax);
  WriteAzimuth(szDegree, this->m_dAziEcliptMax);
  printf("   Eclipt. Max: %s; %s\n", szDeclin, szDegree);

  // Print the Ascendant, Medium Coeli, and Vertex
  for (int i = 0; i < this->m_vAngles.size(); i++) {
    WriteZodicalDegree(
      szDegree,
      ((ChartPoint&)(this->m_vAngles[i])).Longitude,
      false
    );
    WriteDegree(
      szDeclin,
      ((ChartPoint&)(this->m_vAngles[i])).Declination
    );
    printf(
      "%10s: %s; %s\n",
      ((ChartPoint&)(this->m_vAngles[i])).Name,
      szDegree,
      szDeclin
    );
  }

  // Print the Parts
  for (int i = 0; i < this->m_vParts.size(); i++) {
    WriteZodicalDegree(
      szDegree,
      ((ChartPoint&)(this->m_vParts[i])).Longitude,
      false
    );
    WriteDegree(
      szDeclin,
      ((ChartPoint&)(this->m_vParts[i])).Declination
    );
    printf(
      "%10s: %s; %s\n",
      ((ChartPoint&)(this->m_vParts[i])).Name,
      szDegree,
      szDeclin
    );
  }

  // Print the luminaries (and the lunar node)
  for (int i = 0; i < this->m_vLuminaries.size(); i++) {
    blRetro = this->m_vLuminaries[i].LongitudeSpeed < 0;
    WriteZodicalDegree(
      szDegree,
      ((ChartPoint&)(this->m_vLuminaries[i])).Longitude,
      blRetro
    );
    WriteDegree(szDegreeSpeed,
      ((ChartPoint&)(this->m_vLuminaries[i])).LongitudeSpeed
    );
    WriteDegree(
      szDeclin,
      ((ChartPoint&)(this->m_vLuminaries[i])).Declination
    );
    WriteDegree(
      szDeclinSpeed,
      ((ChartPoint&)(this->m_vLuminaries[i])).DeclinationSpeed
    );
    printf(
      "%10s: %s(%s/dy); %s(%s/dy)\n",
      ((ChartPoint&)(this->m_vLuminaries[i])).Name,
      szDegree,
      szDegreeSpeed,
      szDeclin,
      szDeclinSpeed
    );
  }

  // Print the traditional, non-luminary planets
  for (int i = 0; i < this->m_vPlanets.size(); i++) {
    blRetro = this->m_vPlanets[i].LongitudeSpeed < 0;
    WriteZodicalDegree(
      szDegree,
      ((ChartPoint&)(this->m_vPlanets[i])).Longitude,
      blRetro
    );
    WriteDegree(szDegreeSpeed,
      ((ChartPoint&)(this->m_vPlanets[i])).LongitudeSpeed
    );
    WriteDegree(
      szDeclin,
      ((ChartPoint&)(this->m_vPlanets[i])).Declination
    );
    WriteDegree(
      szDeclinSpeed,
      ((ChartPoint&)(this->m_vPlanets[i])).DeclinationSpeed
    );
    printf(
      "%10s: %s(%s/dy); %s(%s/dy)\n",
      ((ChartPoint&)(this->m_vPlanets[i])).Name,
      szDegree,
      szDegreeSpeed,
      szDeclin,
      szDeclinSpeed
    );
  }

  // If indicated, print the outer planets
  if (this->m_blOuters) {
    for (int i = 0; i < this->m_vOuters.size(); i++) {
      blRetro = this->m_vOuters[i].LongitudeSpeed < 0;
      WriteZodicalDegree(
        szDegree,
        ((ChartPoint&)(this->m_vOuters[i])).Longitude,
        blRetro
      );
      WriteDegree(szDegreeSpeed,
        ((ChartPoint&)(this->m_vOuters[i])).LongitudeSpeed
      );
      WriteDegree(
        szDeclin,
        ((ChartPoint&)(this->m_vOuters[i])).Declination
      );
      WriteDegree(
        szDeclinSpeed,
        ((ChartPoint&)(this->m_vOuters[i])).DeclinationSpeed
      );
      printf(
        "%10s: %s(%s/dy); %s(%s/dy)\n",
        ((ChartPoint&)(this->m_vOuters[i])).Name,
        szDegree,
        szDegreeSpeed,
        szDeclin,
        szDeclinSpeed
      );
    }
  }

  // If indicated, print the asteroids
  if (this->m_blAsteroids) {
    for (int i = 0; i < this->m_vAsteroids.size(); i++) {
      blRetro = this->m_vAsteroids[i].LongitudeSpeed < 0;
      WriteZodicalDegree(
        szDegree,
        ((ChartPoint&)(this->m_vAsteroids[i])).Longitude,
        blRetro
      );
      WriteDegree(szDegreeSpeed,
        ((ChartPoint&)(this->m_vAsteroids[i])).LongitudeSpeed
      );
      WriteDegree(
        szDeclin,
        ((ChartPoint&)(this->m_vAsteroids[i])).Declination
      );
      WriteDegree(
        szDeclinSpeed,
        ((ChartPoint&)(this->m_vAsteroids[i])).DeclinationSpeed
      );
      printf(
        "%10s: %s(%s/dy); %s(%s/dy)\n",
        ((ChartPoint&)(this->m_vAsteroids[i])).Name,
        szDegree,
        szDegreeSpeed,
        szDeclin,
        szDeclinSpeed
      );
    }
  }

  // If indicated, print the Uranian planets
  if (this->m_blUranians) {
    for (int i = 0; i < this->m_vUranians.size(); i++) {
      blRetro = this->m_vUranians[i].LongitudeSpeed < 0;
      WriteZodicalDegree(
        szDegree,
        ((ChartPoint&)(this->m_vUranians[i])).Longitude,
        blRetro
      );
      WriteDegree(szDegreeSpeed,
        ((ChartPoint&)(this->m_vUranians[i])).LongitudeSpeed
      );
      WriteDegree(
        szDeclin,
        ((ChartPoint&)(this->m_vUranians[i])).Declination
      );
      WriteDegree(
        szDeclinSpeed,
        ((ChartPoint&)(this->m_vUranians[i])).DeclinationSpeed
      );
      printf(
        "%10s: %s(%s/dy); %s(%s/dy)\n",
        ((ChartPoint&)(this->m_vUranians[i])).Name,
        szDegree,
        szDegreeSpeed,
        szDeclin,
        szDeclinSpeed
      );
    }
  }

  // Release the formatted string allocations
  free(szDegree);
  free(szDegreeSpeed);
  free(szDeclin);
  free(szDeclinSpeed);
}



// Default constructor and destructor for the HelioChart-class
HelioChart::HelioChart() : Chart() {

  // Initialize the traditional planets (excluding luminaries) with
  // Earth included
  this->m_vPlanets = vector<ChartPoint>();
  this->m_vPlanets.push_back(ChartPoint(SE_MERCURY));
  this->m_vPlanets.push_back(ChartPoint(SE_VENUS));
  this->m_vPlanets.push_back(ChartPoint(SE_EARTH));
  this->m_vPlanets.push_back(ChartPoint(SE_MARS));
  this->m_vPlanets.push_back(ChartPoint(SE_JUPITER));
  this->m_vPlanets.push_back(ChartPoint(SE_SATURN));
}
HelioChart::~HelioChart() { }

// Primary constructor for the HelioChart-class with the time provided
HelioChart::HelioChart(DateTime *plTime) : HelioChart() {

  // Set the time of this chart
  this->m_plTime->SetTime(plTime->GetTime());
}

// Function to perform the computations on all points in the chart
void HelioChart::Compute() {

  // Dummy geographic location
  GeographicLocation sLoc;
  double dVal;

  // Go through all of the traditional planets and calculate them
  for (int i = 0; i < this->m_vPlanets.size(); i++) {
    ((ChartPoint&)this->m_vPlanets[i]).Compute(
      this->m_plTime->GetTime(),
      sLoc,
      this->m_nComputeFlags
    );
    dVal = this->m_vPlanets[i].Longitude;
    dVal *= this->m_nHarmonic;
    while (dVal >= 360) { dVal -= 360; }
    this->m_vPlanets[i].Longitude = dVal;
    this->m_vPlanets[i].LongitudeSpeed *= this->m_nHarmonic;
    if (this->m_blDodecatemoria) {
      this->m_vPlanets[i].Longitude =
        ChartPoint::DodecatemoriaDegree(
          this->m_vPlanets[i].Longitude
        );
      this->m_vPlanets[i].LongitudeSpeed *= 12;
    }
    if (this->m_blAntiscia) {
      this->m_vPlanets[i].Longitude =
        ChartPoint::AntisciaDegree(
          this->m_vPlanets[i].Longitude
        );
      this->m_vPlanets[i].LongitudeSpeed *= -1;
    }
    if (this->m_blContraAntiscia) {
      this->m_vPlanets[i].Longitude =
        ChartPoint::ContraAntisciaDegree(
          this->m_vPlanets[i].Longitude
        );
      this->m_vPlanets[i].LongitudeSpeed *= -1;
    }
  }

  // Go through all of the outer planets and calculate them
  for (int i = 0; i < this->m_vOuters.size(); i++) {
    ((ChartPoint&)this->m_vOuters[i]).Compute(
      this->m_plTime->GetTime(),
      sLoc,
      this->m_nComputeFlags
    );
    dVal = this->m_vOuters[i].Longitude;
    dVal *= this->m_nHarmonic;
    while (dVal >= 360) { dVal -= 360; }
    this->m_vOuters[i].Longitude = dVal;
    this->m_vOuters[i].LongitudeSpeed *= this->m_nHarmonic;
    if (this->m_blDodecatemoria) {
      this->m_vOuters[i].Longitude =
        ChartPoint::DodecatemoriaDegree(
          this->m_vOuters[i].Longitude
        );
      this->m_vOuters[i].LongitudeSpeed *= 12;
    }
    if (this->m_blAntiscia) {
      this->m_vOuters[i].Longitude =
        ChartPoint::AntisciaDegree(
          this->m_vOuters[i].Longitude
        );
      this->m_vOuters[i].LongitudeSpeed *= -1;
    }
    if (this->m_blContraAntiscia) {
      this->m_vOuters[i].Longitude =
        ChartPoint::ContraAntisciaDegree(
          this->m_vOuters[i].Longitude
        );
      this->m_vOuters[i].LongitudeSpeed *= -1;
    }
  }

  // Go through all of the asteroids and calculate them
  for (int i = 0; i < this->m_vAsteroids.size(); i++) {
    ((ChartPoint&)this->m_vAsteroids[i]).Compute(
      this->m_plTime->GetTime(),
      sLoc,
      this->m_nComputeFlags
    );
    dVal = this->m_vAsteroids[i].Longitude;
    dVal *= this->m_nHarmonic;
    while (dVal >= 360) { dVal -= 360; }
    this->m_vAsteroids[i].Longitude = dVal;
    this->m_vAsteroids[i].LongitudeSpeed *= this->m_nHarmonic;
    if (this->m_blDodecatemoria) {
      this->m_vAsteroids[i].Longitude =
        ChartPoint::DodecatemoriaDegree(
          this->m_vAsteroids[i].Longitude
        );
      this->m_vAsteroids[i].LongitudeSpeed *= 12;
    }
    if (this->m_blAntiscia) {
      this->m_vAsteroids[i].Longitude =
        ChartPoint::AntisciaDegree(
          this->m_vAsteroids[i].Longitude
        );
      this->m_vAsteroids[i].LongitudeSpeed *= -1;
    }
    if (this->m_blContraAntiscia) {
      this->m_vAsteroids[i].Longitude =
        ChartPoint::ContraAntisciaDegree(
          this->m_vAsteroids[i].Longitude
        );
      this->m_vAsteroids[i].LongitudeSpeed *= -1;
    }
  }

  // Go through all of the uranian planets and calculate them
  for (int i = 0; i < this->m_vUranians.size(); i++) {
    ((ChartPoint&)this->m_vUranians[i]).Compute(
      this->m_plTime->GetTime(),
      sLoc,
      this->m_nComputeFlags
    );
    dVal = this->m_vUranians[i].Longitude;
    dVal *= this->m_nHarmonic;
    while (dVal >= 360) { dVal -= 360; }
    this->m_vUranians[i].Longitude = dVal;
    this->m_vUranians[i].LongitudeSpeed *= this->m_nHarmonic;
    if (this->m_blDodecatemoria) {
      this->m_vUranians[i].Longitude =
        ChartPoint::DodecatemoriaDegree(
          this->m_vUranians[i].Longitude
        );
      this->m_vUranians[i].LongitudeSpeed *= 12;
    }
    if (this->m_blAntiscia) {
      this->m_vUranians[i].Longitude =
        ChartPoint::AntisciaDegree(
          this->m_vUranians[i].Longitude
        );
      this->m_vUranians[i].LongitudeSpeed *= -1;
    }
    if (this->m_blContraAntiscia) {
      this->m_vUranians[i].Longitude =
        ChartPoint::ContraAntisciaDegree(
          this->m_vUranians[i].Longitude
        );
      this->m_vUranians[i].LongitudeSpeed *= -1;
    }
  }
}

// Function to print out the chart
void HelioChart::PrintChart(void) {

  // Print the time information for the chart
  printf("       JD: %lf\n", this->m_plTime->GetTime());

  // Allocate four strings to hold formatted degrees
  // Declare the boolean value to hold whether retrograde
  char *szDegree, *szDegreeSpeed, *szDeclin, *szDeclinSpeed;
  szDegree      = (char*)malloc(sizeof(char) * 128);
  szDegreeSpeed = (char*)malloc(sizeof(char) * 128);
  szDeclin      = (char*)malloc(sizeof(char) * 128);
  szDeclinSpeed = (char*)malloc(sizeof(char) * 128);
  bool blRetro;

  // Print the traditional, non-luminary planets
  for (int i = 0; i < this->m_vPlanets.size(); i++) {
    blRetro = this->m_vPlanets[i].LongitudeSpeed < 0;
    WriteZodicalDegree(
      szDegree,
      ((ChartPoint&)(this->m_vPlanets[i])).Longitude,
      blRetro
    );
    WriteDegree(szDegreeSpeed,
      ((ChartPoint&)(this->m_vPlanets[i])).LongitudeSpeed
    );
    WriteDegree(
      szDeclin,
      ((ChartPoint&)(this->m_vPlanets[i])).Declination
    );
    WriteDegree(
      szDeclinSpeed,
      ((ChartPoint&)(this->m_vPlanets[i])).DeclinationSpeed
    );
    printf(
      "%10s: %s(%s/dy); %s(%s/dy)\n",
      ((ChartPoint&)(this->m_vPlanets[i])).Name,
      szDegree,
      szDegreeSpeed,
      szDeclin,
      szDeclinSpeed
    );
  }

  // If indicated, print the outer planets
  if (this->m_blOuters) {
    for (int i = 0; i < this->m_vOuters.size(); i++) {
      blRetro = this->m_vOuters[i].LongitudeSpeed < 0;
      WriteZodicalDegree(
        szDegree,
        ((ChartPoint&)(this->m_vOuters[i])).Longitude,
        blRetro
      );
      WriteDegree(szDegreeSpeed,
        ((ChartPoint&)(this->m_vOuters[i])).LongitudeSpeed
      );
      WriteDegree(
        szDeclin,
        ((ChartPoint&)(this->m_vOuters[i])).Declination
      );
      WriteDegree(
        szDeclinSpeed,
        ((ChartPoint&)(this->m_vOuters[i])).DeclinationSpeed
      );
      printf(
        "%10s: %s(%s/dy); %s(%s/dy)\n",
        ((ChartPoint&)(this->m_vOuters[i])).Name,
        szDegree,
        szDegreeSpeed,
        szDeclin,
        szDeclinSpeed
      );
    }
  }

  // If indicated, print the asteroids
  if (this->m_blAsteroids) {
    for (int i = 0; i < this->m_vAsteroids.size(); i++) {
      blRetro = this->m_vAsteroids[i].LongitudeSpeed < 0;
      WriteZodicalDegree(
        szDegree,
        ((ChartPoint&)(this->m_vAsteroids[i])).Longitude,
        blRetro
      );
      WriteDegree(szDegreeSpeed,
        ((ChartPoint&)(this->m_vAsteroids[i])).LongitudeSpeed
      );
      WriteDegree(
        szDeclin,
        ((ChartPoint&)(this->m_vAsteroids[i])).Declination
      );
      WriteDegree(
        szDeclinSpeed,
        ((ChartPoint&)(this->m_vAsteroids[i])).DeclinationSpeed
      );
      printf(
        "%10s: %s(%s/dy); %s(%s/dy)\n",
        ((ChartPoint&)(this->m_vAsteroids[i])).Name,
        szDegree,
        szDegreeSpeed,
        szDeclin,
        szDeclinSpeed
      );
    }
  }

  // If indicated, print the Uranian planets
  if (this->m_blUranians) {
    for (int i = 0; i < this->m_vUranians.size(); i++) {
      blRetro = this->m_vUranians[i].LongitudeSpeed < 0;
      WriteZodicalDegree(
        szDegree,
        ((ChartPoint&)(this->m_vUranians[i])).Longitude,
        blRetro
      );
      WriteDegree(szDegreeSpeed,
        ((ChartPoint&)(this->m_vUranians[i])).LongitudeSpeed
      );
      WriteDegree(
        szDeclin,
        ((ChartPoint&)(this->m_vUranians[i])).Declination
      );
      WriteDegree(
        szDeclinSpeed,
        ((ChartPoint&)(this->m_vUranians[i])).DeclinationSpeed
      );
      printf(
        "%10s: %s(%s/dy); %s(%s/dy)\n",
        ((ChartPoint&)(this->m_vUranians[i])).Name,
        szDegree,
        szDegreeSpeed,
        szDeclin,
        szDeclinSpeed
      );
    }
  }

  // Release the formatted string allocations
  free(szDegree);
  free(szDegreeSpeed);
  free(szDeclin);
  free(szDeclinSpeed);
}

